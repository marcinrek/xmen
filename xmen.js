/* 
 * Observe a DOM element - jQuery plugin
 * @author Marcin Rek
 * @version 0.1
 *
 * @param {array} [observers=[]] - observers will be pushed to this array - should be an empty array
 * @param {object || string} [options={attributes: true, childList: true, characterData: true}] - observer options or 'destroy' string when you want to disconnect an observer
 * @param {function} callback - callback function with $(this) provided as value of this
 * @param {array} callbackParams - array with callback function params
 * @param {function} fallback - fallback function if MutationObserver not supported with $(this) provided as value of this
 * @param {array} fallbackParams - array with fallback function params
 * @returns {object} - returns this to enable chaining 
 */
(function ( $ ) {
 
    $.fn.xmen = function(observers, options, callback, callbackParams, fallback, fallbackParams) {
        
        // Fallback if not supported
        if (typeof(MutationObserver) !== "function") { 
            this.toArray().map(function(item, i){
                fallback.apply($(item), fallbackParams);
            });
            return false; 
        }

        // Destroy functionality
        if (Object.prototype.toString.call(observers) === '[object Array]' && options === 'destroy') {
            this.toArray().map(function(_this, i){
                observers[0].map(function(item, j) {
                    if (item.is($(_this))) {
                        observers[1][j].disconnect();
                        observers[1].splice(j,1);
                        observers[0].splice(j,1);
                    }
                });
            });
            return this;
        }

        // Default settings
        var settings = $.extend({ 
            attributes: true, 
            childList: true, 
            characterData: true }, 
        options);

        // Manage observers Array 
        if (Object.prototype.toString.call(observers) !== '[object Array]') {
            console.log('Xmen syntax error - @param {array} [observers=[]] not provided!');
            return this;
        } else {

            if (observers.length === 2 && observers[0][0] !== undefined && observers[1][0] !== undefined) {
                if (!(observers[0][0] instanceof jQuery) || !(typeof observers[1][0].observe === 'function')) {
                    observers.splice(0,observers.length);
                    observers.push([],[]);
                }
            } else {
                observers.splice(0,observers.length);
                observers.push([],[]);
            }
        }
        
        // Create observers
        this.toArray().map(function(item, i){
            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                    callback.apply($(item), callbackParams);
                });    
            });
            observers[0].push($(item));
            observers[1].push(observer);
            observer.observe(item, settings);
        });

        return this;
    };
 
}( jQuery ));