# Xmen #
Observe a DOM element - jQuery plugin

Tested with [jQuery 2.2.4](https://code.jquery.com/jquery-2.2.4.min.js)

## Usage ##

Call:
```
#!javascript
var observersArray = [];
$('.target').xmen(observersArray, {}, callbackFunction, ['callbackFunction', 'params', 'here']);
```
Destroy:

```
#!javascript
$('.target').xmen(observersArray, 'destroy');
```

### Params: ###
* {array} [observers=[]] - observers will be pushed to this array - should be an empty array

* {object || string} [options={attributes: true, childList: true, characterData: true}] - observer options or 'destroy' string when you want to disconnect an observer

* {function} callback - callback function with $(this) provided as value of this

* {array} callbackParams - array with callback function params

* {function} fallback - fallback function if MutationObserver not supported with $(this) provided as value of this 

* {array} fallbackParams - array with fallback function params